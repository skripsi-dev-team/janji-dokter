<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <?php if($this->session->userdata('level') == '1'): ?>
            <a href="<?= site_url('janji/form') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Buat Janji</a>
          <?php endif ?>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered table-custom">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Pasien</th>
                  <th>Spesialis</th>
                  <th width="25%">keterangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($janji as $row): ?>
                <?php if($row->status == 1): ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= setDate($row->tgl) ?></td>
                  <td><?= $row->nomerRegistrasi . ' - '. $row->namaPasien ?></td>
                  <td><?= $row->namaSpesialis ?></td>
                  <td><?= $row->keterangan ?></td>
                  <td>
                    <?php if($this->session->userdata('level') == '2'): ?>
                      <a href="<?= site_url('jadwal/form/').$row->id ?>" class="btn btn-info btn-sm"><i class="fa fa-calendar"></i> Set Jadwal</a>
                    <?php else: ?>
                    <a href="<?= site_url('janji/form/').$row->id ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
                    <button class="btn btn-danger btn-sm delete" data-id="<?= $row->id ?>"><i class="fa fa-trash"></i></button>
                    <?php endif ?>
                  </td>
                </tr>
                <?php endif ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  
  <script>
     $(document).on('click', '.delete', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Hapus Janji?',
        text: "Janji akan terhapus secara permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('janji/destroy') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });
  </script>
  <!-- js -->
</body>

</html>
