<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('dokter/form/'.$dokter->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i> Edit</a>
            </div>
            <div class="card-body">
              <h4>NIP</h4>
              <p><?= $dokter->nip ?></p>
              <h4>Nama</h4>
              <p><?= $dokter->nama ?></p>
              <h4>Jenis Kelamin</h4>
              <p><?= $dokter->jk ?></p>
              <h4>Alamat</h4>
              <p><?= $dokter->alamat ?></p>
              <h4>Telepon</h4>
              <p><?= $dokter->noTelp ?></p>
              <h4>Spesialis</h4>
              <p><?= $dokter->namaSpesialis ?></p>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    
  </script>
  <!-- js -->
</body>

</html>
