<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-4">
          <div class="card bg-success card-costume">
            <div class="card-header">
              <h4><i class="fa fa-calendar-o"></i> Nomer Antrian Berjalan</h4>
            </div>
            <div class="card-body">
              <select name="spesialisId" class="form-control spesialisId mb-2">
                <?php foreach($spesialis as $sps): ?>
                  <option value="<?= $sps->id ?>"><?= $sps->nama ?></option>
                <?php endforeach ?>
              </select>
              <h1 class="nomer"><?= $nomer ?></h1>
            </div>
            <div class="card-footer">
              <p>Tanggal : <?= setDate(date('Y-m-d')) ?></p>
            </div>
          </div>
        </div>
        
        <?php if(isPasien()): ?>
        <div class="col-md-4">
          <div class="card bg-success card-costume">
            <div class="card-header">
              <h4><i class="fa fa-calendar-o"></i> Nomer Antrian Saya</h4>
            </div>
            <div class="card-body">
              <h1><?= $nomerPasien->nomer ?></h1>
              <a href="<?= site_url('jadwal/view/'.$nomerPasien->id) ?>" class="btn btn-light btn-sm <?= (empty($nomerPasien->id))?'disabled':'' ?>">Detail Antrean</a>
            </div>
            <div class="card-footer">
              <p>Tanggal : <?= (empty($nomerPasien->tgl))? '-' : setDate($nomerPasien->tgl) ?></p>
            </div>
          </div>
        </div>
        <?php endif ?>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p>
              Selamat datang pada <b>sistem janji dokter apotek hari nanda</b>. Apotek hari nanda berlokasi di jalan tukad yeh empas no 1 kav 6 BTN sanggulan indah, kecamatan kediri, kabupaten tabanan, bali . Kode pos : 82121. No telp: 081237110712. Buka praktek mulai pukul 18.00 s/d 22.00.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    $(document).ready(function(){
      $('.spesialisId').on('change', function(){
        setNomer();
      });

      setInterval(function(){
        setNomer();
        // console.log('asd');
      }, 5000);

      function setNomer(){
        let id = $('.spesialisId').val();
        $.ajax({
          url: "<?= site_url('jadwal/nomer') ?>",
          type: 'GET',
          data: {
            'id' : id
          },
          success: function(data){
            $('.nomer').empty();
            $('.nomer').append(data);
          }
        });
      }
    });
  </script>
  <!-- js -->
</body>

</html>
