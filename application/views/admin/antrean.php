<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Antrian</title>
    <style type="text/css">
      body{
        margin: 0;
        padding: 0;
        font-size: 4em;
      }

      .wreapper{
        width: 100%;
        height: 100vh;
      }

      .header{
        background: #3498db;
        font-size: .7em;
        color: white;
      }

      .content{
        font-size: .6em;
      }

      .row{
        padding: 0;
        margin: 0;
      }

      footer{
        font-size: .5em;
        text-align: left;
        position: absolute;
        bottom: 0;
        background: #3498db;
        padding: 1%;
        color: white;
        width: 100%;
      }
    </style>
  </head>
  <body>
    <div class="wreapper text-center">
      <div class="header">
        APOTEK <?= APP_NAME ?>
      </div>
      <div class="content">
        <p>Nomer Antrian :</p>
        <div class="row justify-content-center">
          <?php $i = 0; ?>
          <?php foreach($spesialis as $sps): ?>
            <div class="col-md-4">
              <div class="card text-white bg-primary">
                <div class="card-header">
                  <b><?= $sps->nama ?></b>
                </div>
                <div class="card-body">
                  <p class="nomer<?= $i ?>"><?= $nomer[$i] ?></p>
                </div>
              </div>
            </div>
          <?php $i++; endforeach ?>
        </div>
      </div>
      <footer>
        Tanggal : <?= date('d F Y') ?>
      </footer>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>
      $(document).ready(function(){
        setInterval(function(){
          setNomer();
        }, 5000);
        function setNomer(){
          $.ajax({
            url: "<?= site_url('admin/auth/setnomer') ?>",
            type: 'GET',
            dataType: 'json',
            success: function(data){
              for(let i=0; i<data.length; i++){
                $('.nomer'+i).empty();
                $('.nomer'+i).append(data[i]);
              }
            }
          });
        }
      });
    </script>
  </body>
</html>