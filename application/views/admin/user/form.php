<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('admin/user') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-6">
              <form action="<?= (empty($user->id))? site_url('admin/user/store') : site_url('admin/user/update') ?>" method="POST">
                <?php if(!empty($user->id)): ?>
                <input type="hidden" name="id" value="<?= $user->id ?>">
                <?php endif ?>
                
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" name="username" class="form-control" id="username" value="<?= $user->username ?>">

                  <small class="text-danger"><?= form_error('username') ?></small>
                </div>

                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" name="nama" class="form-control" id="nama" value="<?= $user->nama ?>">

                  <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <!-- js -->
</body>

</html>
