<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('admin/spesialis') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-6">
              <form action="<?= (empty($spesialis->id))? site_url('admin/spesialis/store') : site_url('admin/spesialis/update') ?>" method="POST">
                <?php if(!empty($spesialis->id)): ?>
                <input type="hidden" name="id" value="<?= $spesialis->id ?>">
                <?php endif ?>

                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" name="nama" class="form-control" id="nama" value="<?= $spesialis->nama ?>">

                  <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                  <label for="inisial">Inisial</label>
                  <input type="text" name="inisial" class="form-control" id="inisial" value="<?= $spesialis->inisial ?>">

                  <small class="text-danger"><?= form_error('inisial') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <!-- js -->
</body>

</html>
