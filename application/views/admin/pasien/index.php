<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('admin/pasien/form') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Tambah</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered table-custom">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nomer</th>                 
                  <th>Nama</th>
                  <th>TTL</th>
                  <th>JK</th>
                  <th width="20%">Alamat</th>
                  <th>Telepon</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($pasien as $row): ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $row->nomerRegistrasi ?></td>
                  <td><?= $row->nama ?></td>
                  <td><?= $row->tmptLahir.', '.setDate($row->tglLahir) ?></td>
                  <td><?= $row->jk ?></td>
                  <td><?= $row->alamat ?></td>
                  <td><?= $row->noTelp ?></td>
                  <td>
                    <button class="btn btn-warning btn-sm reset" data-id="<?= $row->id ?>"><i class="fa fa-key"></i></button>
                    <a href="<?= site_url('admin/pasien/form/').$row->id ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
                    <button class="btn btn-danger btn-sm delete" data-id="<?= $row->id ?>"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  
  <script>
     $(document).on('click', '.delete', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Hapus Data User?',
        text: "Data akan terhapus secara permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('admin/pasien/destroy') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });

    $(document).on('click', '.reset', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Reset password pasien?',
        text: "Password akan terset default 12345",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('admin/pasien/reset') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });
  </script>
  <!-- js -->
</body>

</html>
