<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('admin/jadwal') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
              <a href="<?= site_url('admin/jadwal/printantrean/'.$jadwal->id) ?>" class="btn btn-primary btn-sm" target="_BLANK"><i class="fa fa-print"></i> Print Nomer</a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-calendar"></i> Data Pasien</h5>
            </div>
            <div class="card-body">
              <h6>Tanggal Janji</h6>
              <p><?= setDate($jadwal->tglJanji) ?></p>
              <h6>Nama Pasien</h6>
              <p><?= $jadwal->namaPasien ?></p>
              <h6>Spesialis Terkait</h6>
              <p><?= $jadwal->namaSpesialis ?></p>
              <h6>Keluhan</h6>
              <p><?= $jadwal->keterangan ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-calendar"></i> Jadwal Periksa</h5>
            </div>
            <div class="card-body">
              	<h6>Nama Dokter</h6>
              	<p><?= $jadwal->namaDokter ?></p>
              	<h6>Spesialis Terkait</h6>
              	<p><?= $jadwal->namaSpesialis ?></p>
              	<h6>Tanggal Pemeriksaan</h6>
				<p><?= setDate($jadwal->tgl) ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    
  </script>
  <!-- js -->
</body>

</html>
