<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('admin/jadwal') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-calendar"></i> Data Janji</h5>
            </div>
            <div class="card-body">
              <h6>Tanggal</h6>
              <p><?= setDate($janji->tgl) ?></p>
              <h6>Nama Pasien</h6>
              <p><?= $janji->namaPasien ?></p>
              <h6>Spesialis Terkait</h6>
              <p><?= $janji->namaSpesialis ?></p>
              <h6>Keterangan</h6>
              <p><?= $janji->keterangan ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-calendar"></i> Penjadwalan Pasien</h5>
            </div>
            <div class="card-body">
              <form action="<?= (empty($jadwal->id))? site_url('admin/jadwal/store') : site_url('admin/jadwal/update') ?>" method="POST">
                <?php if(!empty($jadwal->id)): ?>
                <input type="hidden" name="id" value="<?= $jadwal->id ?>">
                <input type="hidden" name="nomer" value="<?= $jadwal->nomer ?>">
                <?php endif ?>
                
                <input type="hidden" name="janjiId" value="<?= $janji->id ?>">
                <div class="form-group">
                  <label for="tgl">Tanggal</label>
                  <input type="date" name="tgl" class="form-control" id="tgl" min="<?= date('Y-m-d') ?>" value="<?= $jadwal->tgl ?>">
                  <input type="hidden" name="tglOld" class="form-control" value="<?= $jadwal->tgl ?>">
                  
                  <small class="text-info">Tidak bisa memilih tanggal sebelum hari ini.</small>
                  <small class="text-danger"><?= form_error('tgl') ?></small>
                </div>

                <div class="form-group">
                  <label for="dokterId">Dokter</label>
                  <select name="dokterId" id="dokterId" class="form-control">
                    <?php if(!empty($jadwal->id)): ?>
                      <option value="<?= $jadwal->dokterId ?>" selected><?= $jadwal->namaDokter ?></option>
                    <?php endif ?>
                  </select>
                    
                  <small class="text-info">Pencarian dokter sudah sesuai dengan spesialis.</small>
                  <small class="text-danger"><?= form_error('dokterId') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Buat jadwal</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    $('#dokterId').select2({
      placeholder: 'Cari Dokter...',
      theme: "bootstrap",
      ajax: {
        url: "<?= site_url('admin/dokter/loadDokterForm/').$janji->spesialisId ?>",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: $.map(data, function(item){
              return {
                text: item.nama,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
  </script>
  <!-- js -->
</body>

</html>
