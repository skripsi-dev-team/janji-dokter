<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('pasien/form/'.$pasien->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i> Edit</a>
            </div>
            <div class="card-body">
              <h4>Nomer Registrasi</h4>
              <p><?= $pasien->nomerRegistrasi ?></p>
              <h4>NIK</h4>
              <p><?= $pasien->nik ?></p>
              <h4>Nama</h4>
              <p><?= $pasien->nama ?></p>
              <h4>Tempat, Tanggal Lahir</h4>
              <p><?= $pasien->tmptLahir.', '.setDate($pasien->tglLahir) ?></p>
              <h4>Jenis Kelamin</h4>
              <p><?= $pasien->jk ?></p>
              <h4>Alamat</h4>
              <p><?= $pasien->alamat ?></p>
              <h4>Telepon</h4>
              <p><?= $pasien->noTelp ?></p>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    
  </script>
  <!-- js -->
</body>

</html>
