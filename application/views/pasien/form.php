<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('pasien') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <form action="<?= (empty($pasien->id))? site_url('pasien/store') : site_url('pasien/update') ?>" method="POST">
                <?php if(!empty($pasien->id)): ?>
                <input type="hidden" name="id" value="<?= $pasien->id ?>">
                <?php endif ?>
                
             

                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" name="nama" class="form-control" id="nama" value="<?= $pasien->nama ?>">

                  <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                  <label>TTL</label>
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" name="tmptLahir" class="form-control" id="tmptLahir" value="<?= $pasien->tmptLahir ?>" placeholder="Tempat">

                      <small class="text-danger"><?= form_error('tmptLahir') ?></small>
                    </div>
                    <div class="col-md-6">
                      <input type="date" name="tglLahir" class="form-control" id="tglLahir" value="<?= $pasien->tglLahir ?>">

                      <small class="text-danger"><?= form_error('tmptLahir') ?></small>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="jk">Jenis Kelamin</label>
                  <select name="jk" id="jk" class="form-control">
                    <option value="">Pilih</option>
                    <option value="L" <?= ($pasien->jk == 'L')?'selected':'' ?>>Laki - laki</option>
                    <option value="P" <?= ($pasien->jk == 'P')?'selected':'' ?>>Perempuan</option>
                  </select>

                  <small class="text-danger"><?= form_error('jk') ?></small>
                </div>

                <div class="form-group">
                  <label for="noTelp">Nomer Telepon</label>
                  <input type="text" name="noTelp" class="form-control" id="noTelp" value="<?= $pasien->noTelp ?>">

                  <small class="text-danger"><?= form_error('noTelp') ?></small>
                </div>

                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea name="alamat" id="alamat" rows="5" class="form-control"><?= $pasien->alamat ?></textarea>

                  <small class="text-danger"><?= form_error('alamat') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <!-- js -->
</body>

</html>
