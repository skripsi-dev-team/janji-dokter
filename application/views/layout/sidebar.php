<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
      <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="./"><h3><?= APP_NAME ?></h3></a>
        <a class="navbar-brand hidden" href="./">HN</a>
      </div>

      <div id="main-menu" class="main-menu collapse navbar-collapse">
        <?php if($this->session->userdata('level') == '3'): ?>
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="<?= site_url('admin/dashboard') ?>"> <i class="menu-icon fa fa-dashboard text-primary"></i>Dashboard </a>
          </li>

          <h3 class="menu-title">Data Master</h3>
          <li>
            <a href="<?= site_url('admin/user') ?>"> <i class="menu-icon fa fa-user text-info"></i>Admin </a>
          </li>
          <li>
            <a href="<?= site_url('admin/pasien') ?>"> <i class="menu-icon fa fa-user-md text-info"></i>Pasien </a>
          </li>
          <li>
            <a href="<?= site_url('admin/dokter') ?>"> <i class="menu-icon fa fa-stethoscope text-success"></i>Dokter </a>
          </li>
          <li>
            <a href="<?= site_url('admin/spesialis') ?>"> <i class="menu-icon fa fa-medkit text-primary"></i>Spesialis </a>
          </li>

          <h3 class="menu-title">Fitur</h3>
          <li>
            <a href="<?= site_url('admin/janji') ?>"> <i class="menu-icon ti-timer text-warning"></i>Janji </a>
          </li>
          <li>
            <a href="<?= site_url('admin/jadwal') ?>"> <i class="menu-icon ti-calendar text-info"></i>Jadwal </a>
          </li>
        </ul>
        <?php else: ?>
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="<?= site_url('dashboard') ?>"> <i class="menu-icon fa fa-dashboard text-primary"></i>Dashboard </a>
          </li>

          <h3 class="menu-title">Fitur</h3>
          <li>
            <a href="<?= site_url('janji') ?>"> <i class="menu-icon ti-timer text-warning"></i>Janji </a>
          </li>
          <li>
            <a href="<?= site_url('jadwal') ?>"> <i class="menu-icon ti-calendar text-info"></i>Jadwal </a>
          </li>

          <h3 class="menu-title">Laporan</h3>
          <li>
            <a href="<?= site_url('rekammedis') ?>"> <i class="menu-icon fa fa-h-square text-info"></i>Rekam Medis </a>
          </li>
        </ul>
        <?php endif ?>
      </div><!-- /.navbar-collapse -->
    </nav>
  </aside><!-- /#left-panel -->