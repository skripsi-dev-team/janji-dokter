<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?= $title ?> - Hari Nanda</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon" href="apple-icon.png">
<link rel="shortcut icon" href="favicon.ico">

<link rel="stylesheet" href="<?= base_url('vendors/bootstrap/dist/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/font-awesome.min.css')?>">
<link rel="stylesheet" href="<?= base_url('vendors/themify-icons/css/themify-icons.css')?>">
<link rel="stylesheet" href="<?= base_url('vendors/flag-icon-css/css/flag-icon.min.css')?>">
<link rel="stylesheet" href="<?= base_url('vendors/selectFX/css/cs-skin-elastic.css')?>">
<link rel="stylesheet" href="<?= base_url('vendors/jqvmap/dist/jqvmap.min.css')?>">

<link rel="stylesheet" href="<?= base_url('vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>">

<link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>">
<link rel="stylesheet" href="<?= base_url('assets/css/app.css')?>">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet">