<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Rekam Medis</title>

    <style type="text/css">
      body{
        font-size: .8em;
      }
    </style>
  </head>
  <body>
    <h1 class="text-center">Rekam Medis</h1>
    <hr>

    <h4>Data Pasien</h4>
    <h6>Nama</h6>
    <p><?= $pasien->nama ?></p>
    <h6>TTL</h6>
    <p><?= $pasien->tmptLahir.', '.setDate($pasien->tglLahir) ?></p>
    <h6>Jenis Kelamin</h6>
    <p><?= $pasien->jk ?></p>
    <h6>Telepon</h6>
    <p><?= $pasien->noTelp ?></p>
    <h6>Alamat</h6>
    <p><?= $pasien->alamat ?></p>

    <hr>

    <h4>Catatan Rekam Medis</h4>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>NIP</th>
          <th>Nama Dokter</th>
          <th>Spesialis</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($rekamMedis as $rm): ?>
        <tr>
          <td rowspan="2"><?= $no++ ?></td>
          <td><?= setDate($rm->tgl) ?></td>
          <td><?= $rm->nip ?></td>
          <td><?= $rm->namaDokter ?></td>
          <td><?= $rm->namaSpesialis ?></td>
        </tr>
        <tr>
          <td colspan="4">
            <h6>Catatan</h6>
            <?= $rm->catatan ?>

            <h6>Obat</h6>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Obat</th>
                  <th>Qty</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $CI =& get_instance();
                  $CI->load->model('Obat_model', 'obat');
                ?>
                <?php $no2 = 1; foreach($CI->obat->findList($rm->id) as $row): ?>
                  <tr>
                    <td><?= $no2++ ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->qty ?></td>
                    <td><?= $row->keterangan ?></td>
                  </tr>
                <?php endforeach ?> 
              </tbody>
            </table>
          </td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>