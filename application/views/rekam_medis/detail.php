<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('rekammedis') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
              <a href="<?= site_url('rekammedis/print/'.$pasien->id) ?>" class="btn btn-primary btn-sm" target="_BLANK"><i class="fa fa-print"></i> Print</a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-user-md"></i> Data Pasien</h5>
            </div>
            <div class="card-body">
              <h6>Nama</h6>
              <p><?= $pasien->nama ?></p>
              <h6>TTL</h6>
              <p><?= $pasien->tmptLahir.', '.setDate($pasien->tglLahir) ?></p>
              <h6>Jenis Kelamin</h6>
              <p><?= $pasien->jk ?></p>
              <h6>Telepon</h6>
              <p><?= $pasien->noTelp ?></p>
              <h6>Alamat</h6>
              <p><?= $pasien->alamat ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-hospital-o"></i> Rekam Medis</h5>
            </div>
            <div class="card-body">
              <table id="bootstrap-data-table-export" class="table table-striped table-bordered table-custom">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>NIP</th>
                    <th>Dokter</th>
                    <th>Spesialis</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($rekamMedis as $rm): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= setDate($rm->tgl) ?></td>
                    <td><?= $rm->nip ?></td>
                    <td><?= $rm->namaDokter ?></td>
                    <td><?= $rm->namaSpesialis ?></td>
                    <td>
                      <button class="btn btn-info btn-sm detailModal" data-id="<?= $rm->id ?>" data-toggle="modal" data-target="#detailModal"><i class="fa fa-eye"></i></button>

                      <?php if(isDokter()): ?>
                        <a href="<?= site_url('rekammedis/form/').$rm->jadwalId.'/'.$rm->id ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
                      <?php endif ?>
                    </td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- modal -->
  <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="detailModalLabel">Catatan Rekam Medis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="contentDetail"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    $(document).on('click', '.detailModal', function(){
      var id = $(this).data('id');
      
      $.ajax({
        type: 'GET',
        url: '<?= site_url('rekammedis/view') ?>',
        data: {
          'id': id
        },
        success: function(data){
          $('#contentDetail').empty();
          $('#contentDetail').append(data);
        }
      });
    });
  </script>
  <!-- js -->
</body>

</html>
