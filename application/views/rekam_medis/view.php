<h4>Catatan</h4>
<p><?= $rekamMedis->catatan ?></p>

<h4>Obat</h4>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>No</th>
      <th>Obat</th>
      <th>Qty</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1; foreach($obat as $row): ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->qty ?></td>
        <td><?= $row->keterangan ?></td>
      </tr>
    <?php endforeach ?> 
  </tbody>
</table>

