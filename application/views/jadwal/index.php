<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6">
              <a href="<?= site_url('admin/janji') ?>" class="btn btn-primary btn-sm"><i class="menu-icon ti-timer"></i> Lihat Janji</a>
            </div>
            <div class="col-md-6">
              <form method="GET" class="form-filter">
                <?php 
                  $tgl = '';
                  $status = '';
                  if($_GET){
                    $tgl = $_GET['tgl'];
                    $status = $_GET['status'];
                  }
                ?>
                <div class="row">
                  <div class="col-md-6">
                    <select name="status" class="form-control">
                      <option value="">Pilih Status</option>
                      <option value="1" <?= ($status == '1')?'selected':'' ?>>Pending</option>
                      <option value="0" <?= ($status == '0')?'selected':'' ?>>Selesai</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <input type="date" name="tgl" class="form-control" value="<?= $tgl ?>">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered table-custom">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomer</th>
                  <th>Pasien</th>
                  <th>Dokter</th>
                  <th>Spesialis</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($jadwal as $row): ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= setDate($row->tgl) ?></td>
                  <td><?= $row->nomer ?></td>
                  <td><?= $row->nomerRegistrasi . ' - '. $row->namaPasien ?></td>
                  <td><?= $row->namaDokter ?></td>
                  <td><?= $row->namaSpesialis ?></td>
                  <td><span class="badge badge-success"><?= status($row->status) ?></span></td>
                  <td>
                    <?php if($row->status == '1'): ?>
                      <?php if($this->session->userdata('level') == '2'): ?>
                        <a href="<?= site_url('rekammedis/form/').$row->id ?>" class="btn btn-primary btn-sm"><i class="fa fa-hospital-o"></i> Rekam Medis</a>
                      <?php endif ?>
                      
                      <a href="<?= site_url('jadwal/view/'.$row->id) ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>

                      <?php if(isDokter()): ?>
                      <a href="<?= site_url('jadwal/form/').$row->janjiId.'/'.$row->id ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
                      <button class="btn btn-danger btn-sm delete" data-id="<?= $row->id ?>"><i class="fa fa-trash"></i></button>
                      <?php endif ?>
                    <?php endif ?>
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- modal -->
  <!-- Modal -->
  <div class="modal fade" id="jadwalModal" tabindex="-1" role="dialog" aria-labelledby="jadwalModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="jadwalModalLabel">Jadwal Kunjungan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="contentJadwal"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  
  <script>
     $(document).on('click', '.delete', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Hapus Data Jadwal?',
        text: "Data akan terhapus secara permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('jadwal/destroy') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });

    $(document).on('click', '.jadwalModal', function(){
      var id = $(this).data('id');
      
      $.ajax({
        type: 'GET',
        url: '<?= site_url('jadwal/view') ?>',
        data: {
          'id': id
        },
        success: function(data){
          $('#contentJadwal').empty();
          $('#contentJadwal').append(data);
        }
      });
    });
  </script>
  <!-- js -->
</body>

</html>
