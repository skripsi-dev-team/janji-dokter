<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('Pasien_model', 'pasien');
    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Spesialis_model', 'spesialis');
  }

	public function index(){
    if(!empty($this->jadwal->nomerAntrian())){
      $nomer = $this->jadwal->nomerAntrian();
      $jmlPasien = $this->jadwal->jmlPasien();
      $data['nomer'] = $nomer->nomer;
      $data['jmlPasien'] = count($jmlPasien);
    }else{
      $data['nomer'] = '-';
      $data['jmlPasien'] = '0';
    }
    $data['spesialis'] = $this->spesialis->showAll();

		$this->load->view('login', $data);
  }

  public function login(){
    $post = $this->input->post();
    $validation = $this->form_validation;

    if($post['type'] == '1'){
      $pasien = $this->pasien;
      $validation->set_rules($pasien->rulesLogin());
      if($validation->run()){
        $data = $pasien->login();

        if(!empty($data)){
          $array = array(
            "id" => $data->id,
            "nomerRegistrasi" => $data->nomerRegistrasi,
            "nama" => $data->nama,
            "level" => 1,
            "login" => true
          );

          $this->session->set_userdata($array);
          flashData('success', 'Selamat datang pasien '.$data->nama.'.');

          return redirect('dashboard');
        }else{
          flashData('danger', 'Nomer registrasi dan password tidak valid!');

          return redirect('/');
        }
      }
    }else{
      $dokter = $this->dokter;
      $validation->set_rules($dokter->rulesLogin());
      if($validation->run()){
        $data = $dokter->login();

        if(!empty($data)){
          $array = array(
            "id" => $data->id,
            "nip" => $data->nip,
            "nama" => $data->nama,
            "spesialisId" => $data->spesialisId,
            "level" => 2,
            "login" => true
          );

          $this->session->set_userdata($array);
          flashData('success', 'Selamat datang dokter '.$data->nama.', selamat bekerja.');

          return redirect('dashboard');
        }else{
          flashData('danger', 'NIP dan password tidak valid!');

          return redirect('/');
        }
      }
    }
    
  }

  public function logoutPasien(){
    cekLoginUser();
    $array = array('id', 'nik', 'nama', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('/');
  }

  public function logoutDokter(){
    cekLoginUser();
    $array = array('id', 'nip', 'nama', 'spesialisId', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('/');
  }

  public function nomer(){
    $nomer = $this->jadwal->nomerAntrian();
    if(!empty($nomer)){
      echo $nomer->nomer;
    }else{
      echo '-';
    }
  }

  public function antrian(){
    $antrian = $this->jadwal->jmlPasien();
    if(!empty($antrian)){
      echo count($antrian);
    }else{
      echo '0';
    }
  }
}
