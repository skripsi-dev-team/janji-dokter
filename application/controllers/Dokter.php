<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginUser();

    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('Spesialis_model', 'spesialis');
  }

	public function view(){
    $data['title'] = 'Dokter';
    $data['no'] = 1;
    $data['dokter'] = $this->dokter->findData($this->session->userdata('id'));
    
		$this->load->view('dokter/view', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Dokter';
    $data['spesialis'] = $this->spesialis->showAll();

    if(empty($id)){
      $data['dokter'] = (object)[
        'id' => '',
        'nip' => '',
        'nama' => '',
        'jk' => '',
        'alamat' => '',
        'noTelp' => ''
      ];
    }else{
      $data['dokter'] = $this->dokter->find($id);
    }
    
    $this->load->view('dokter/form', $data);
  }

  public function update(){
    $dokter = $this->dokter;
    $validation = $this->form_validation;
    $validation->set_rules($dokter->rules());
    
    if($validation->run()){
      $dokter->update();
      flashData('success', 'Update data berhasil.');

      return redirect('dokter/view');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function loadDokterForm($spesialisId){
    $json = [];
    $key = '';

    if(!empty($this->input->get('q'))){
      $key = $this->input->get('q');
    }

    $data = $this->db->select('id, nama')->like('nama', $key)->where('spesialisId', $spesialisId)->get('dokter')->result();
    echo json_encode($data);
  }

  public function repassword(){
    $dokter = $this->dokter;
    $validation = $this->form_validation;
    $validation->set_rules($dokter->rulesRePassword());
    if($validation->run()){
      if($dokter->checkPassword()){
        $dokter->rePassword();
        flashData('success', 'Silahkan masuk kembali.');

        return redirect('auth/logoutdokter');
      }
      
      flashData('warning', 'Password lama tidak sama!');
      return redirect('dashboard');
    }

    flashData('danger', 'Data tidak valid!');
    return redirect('dashboard');
  }
}
