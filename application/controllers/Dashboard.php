<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Spesialis_model', 'spesialis');

    cekLoginUser();
  }

	public function index(){
    $data['title'] = 'Dashboard';
    $data['spesialis'] = $this->spesialis->showAll();

    if(!empty($this->jadwal->nomerAntrian())){
      $nomer = $this->jadwal->nomerAntrian();
      $data['nomer'] = $nomer->nomer;
    }else{
      $data['nomer'] = '-';
    }

    if($this->session->userdata('level') == '1'){
      if(!empty($this->jadwal->nomerAntrianPasien($this->session->userdata('id')))){
        $nomerPasien = $this->jadwal->nomerAntrianPasien($this->session->userdata('id'));
        $data['nomerPasien'] = $nomerPasien;
      }else{
        $data['nomerPasien'] = (object)[
          'id' => '',
          'tgl' => '',	      
          'nomer' => '-'      
        ];
      }
    }

    $this->load->view('dashboard/index', $data);
  }
}
