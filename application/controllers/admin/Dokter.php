<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('Spesialis_model', 'spesialis');
  }

	public function index(){
    $data['title'] = 'Dokter';
    $data['no'] = 1;
    $data['dokter'] = $this->dokter->showAll();
    $data['spesialis'] = $this->spesialis->showAll();
    
		$this->load->view('admin/dokter/index', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Dokter';
    $data['spesialis'] = $this->spesialis->showAll();

    if(empty($id)){
      $data['dokter'] = (object)[
        'id' => '',
        'nip' => '',
        'nama' => '',
        'jk' => '',
        'alamat' => '',
        'noTelp' => '',
        'spesialisId' => ''
      ];
    }else{
      $data['dokter'] = $this->dokter->find($id);
    }
    
    $this->load->view('admin/dokter/form', $data);
  }

  public function store(){
    $dokter = $this->dokter;
    $validation = $this->form_validation;
    $validation->set_rules($dokter->rules());
    
    if($validation->run()){
      $dokter->store();
      flashData('success', 'Simpan data berhasil.');

      return redirect('admin/dokter');
    }

    flashData('danger', 'Simpan data gagal!');
    return $this->form();
  }

  public function update(){
    $dokter = $this->dokter;
    $validation = $this->form_validation;
    $validation->set_rules($dokter->rules());
    
    if($validation->run()){
      $dokter->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/dokter');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function reset(){
    $this->dokter->resetPassword();
  }

  public function destroy(){
    $this->dokter->destroy();
  }

  public function loadDokterForm($spesialisId){
    $json = [];
    $key = '';

    if(!empty($this->input->get('q'))){
      $key = $this->input->get('q');
    }

    $data = $this->db->select('id, nama')->like('nama', $key)->where('spesialisId', $spesialisId)->get('dokter')->result();
    echo json_encode($data);
  }
}
