<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Janji extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('Janji_model', 'janji');
    $this->load->model('Spesialis_model', 'spesialis');
    $this->load->model('Pasien_model', 'pasien');
  }

	public function index(){
    $data['title'] = 'Janji';
    $data['no'] = 1;
    $data['janji'] = $this->janji->showAll();
    
		$this->load->view('admin/janji/index', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Janji';
    $data['spesialis'] = $this->spesialis->showAll();

    if(empty($id)){
      $data['janji'] = (object)[
        'id' => '',
        'tgl' => date('Y-m-d'),
        'keterangan' => '',
        'pasienId' => '',
        'spesialisId' => ''
      ];
    }else{
      $data['janji'] = $this->janji->find($id);
    }
    
    $this->load->view('admin/janji/form', $data);
  }

  public function store(){
    $janji = $this->janji;
    $validation = $this->form_validation;
    $validation->set_rules($janji->rules());
    
    if($validation->run()){
      $janji->store();
      flashData('success', 'Janji berhasil dibuat.');

      return redirect('admin/janji');
    }

    flashData('danger', 'Janji gagal dibuat!');
    return $this->form();
  }

  public function update(){
    $janji = $this->janji;
    $validation = $this->form_validation;
    $validation->set_rules($janji->rules());
    
    if($validation->run()){
      $janji->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/janji');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function reset(){
    $this->dokter->resetPassword();
  }

  public function destroy(){
    $this->janji->destroy();
  }
}
