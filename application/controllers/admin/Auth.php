<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('User_model', 'user');
    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Spesialis_model', 'spesialis');
  }

	public function index(){
		$this->load->view('admin/login');
  }

  public function antrean(){
    $spesialis = $this->spesialis->showAll();
    $array = [];
    
    foreach($spesialis as $sps){
      $nomer = $this->db->select('jadwal.id, tgl, nomer, dokter.spesialisId')
                      ->from('jadwal')
                      ->join('dokter', 'dokter.id = jadwal.dokterId')
                      ->where('dokter.spesialisId', $sps->id)
                      ->where('tgl', date('Y-m-d'))
                      ->where('jadwal.status', '1')
                      ->order_by('jadwal.id', 'asc')
                      ->get()
                      ->row();
      if(!empty($nomer)){
        array_push($array, $nomer->nomer);
      }else{
        array_push($array, '-');
      }
    }

    $data['spesialis'] = $spesialis;
    $data['nomer'] = $array;

    $this->load->view('admin/antrean', $data);
  }

  public function setnomer(){
    $spesialis = $this->spesialis->showAll();
    $array = [];
    
    foreach($spesialis as $sps){
      $nomer = $this->db->select('jadwal.id, tgl, nomer, dokter.spesialisId')
                      ->from('jadwal')
                      ->join('dokter', 'dokter.id = jadwal.dokterId')
                      ->where('dokter.spesialisId', $sps->id)
                      ->where('tgl', date('Y-m-d'))
                      ->where('jadwal.status', '1')
                      ->order_by('jadwal.id', 'asc')
                      ->get()
                      ->row();
      if(!empty($nomer)){
        array_push($array, $nomer->nomer);
      }else{
        array_push($array, '-');
      }
    }

    echo json_encode($array);
  }

  public function login(){
    $user = $this->user;
    $validation = $this->form_validation;

    $validation->set_rules($user->rulesLogin());
    if($validation->run()){
      $data = $user->login();

      if(!empty($data)){
        $array = array(
          "id" => $data->id,
          "username" => $data->username,
          "nama" => $data->nama,
          "level" => 3,
          "login" => true
        );

        $this->session->set_userdata($array);
        flashData('success', 'Selamat datang '.$data->nama.', selamat bekerja');

        return redirect('admin/dashboard');
      }else{
        flashData('danger', 'Username dan password tidak valid!');

        return redirect('admin/');
      }
    }
  }

  public function logout(){
    cekLoginAdmin();
    $array = array('id', 'username', 'nama', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('admin/');
  }
}
