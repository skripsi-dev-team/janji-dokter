<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('User_model', 'user');
  }

	public function index(){
    $data['title'] = 'Admin';
    $data['no'] = 1;
    $data['user'] = $this->user->showAll();
    
		$this->load->view('admin/user/index', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Admin';

    if(empty($id)){
      $data['user'] = (object)[
        'id' => '',
        'username' => '',
        'nama' => ''
      ];
    }else{
      $data['user'] = $this->user->find($id);
    }
    
    $this->load->view('admin/user/form', $data);
  }

  public function repassword(){
    $user = $this->user;
    $validation = $this->form_validation;
    $validation->set_rules($user->rulesRePassword());
    if($validation->run()){
      if($user->checkPassword()){
        $user->rePassword();
        flashData('success', 'Silahkan masuk kembali.');
        return redirect('admin/logout');
      }
      
      flashData('warning', 'Password lama tidak sama!');
      return redirect('admin/dashboard');
    }

    flashData('danger', 'Data tidak valid!');
    return redirect('admin/dashboard');
  }

  public function store(){
    $user = $this->user;
    $validation = $this->form_validation;
    $validation->set_rules($user->rules());
    
    if($validation->run()){
      $user->store();
      flashData('success', 'Simpan data berhasil.');

      return redirect('admin/user');
    }

    flashData('danger', 'Simpan data gagal!');
    return $this->form();
  }

  public function update(){
    $user = $this->user;
    $validation = $this->form_validation;
    $validation->set_rules($user->rules());
    
    if($validation->run()){
      $user->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/user');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function destroy(){
    $this->user->destroy();
  }
}
