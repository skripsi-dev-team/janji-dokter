<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Pasien_model', 'pasien');
		$this->load->model('Dokter_model', 'dokter');
		$this->load->model('Spesialis_model', 'spesialis');

    cekLoginAdmin();
  }

	public function index(){
    $data['title'] = 'Dashboard';

    if(!empty($this->jadwal->nomerAntrian())){
	    $nomer = $this->jadwal->nomerAntrian();
	    $data['nomer'] = $nomer->nomer;
	  }else{
	    $data['nomer'] = '-';
	  }

		$data['spesialis'] = $this->spesialis->showAll();
	  $data['jmlPasien'] = $this->pasien->countData();
	  $data['jmlDokter'] = $this->dokter->countData();

		$this->load->view('admin/dashboard/index', $data);
  }
}
