<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter_model extends CI_Model {

  public $nip;
  public $password;
  public $nama;
  public $jk;
  public $noTelp;
  public $alamat;
  public $spesialisId;

	public function rules(){
    $id = '|is_unique[dokter.nip]';
    if(!empty($this->input->post('id'))){
      $id = '';
    }

    return [
      [
        'field' => 'nip',
        'label' => 'nip',
        'rules' => 'required|numeric'.$id
      ],
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ],
      [
        'field' => 'jk',
        'label' => 'jk',
        'rules' => 'required'
      ],
      [
        'field' => 'alamat',
        'label' => 'alamat',
        'rules' => 'required'
      ],
      [
        'field' => 'noTelp',
        'label' => 'noTelp',
        'rules' => 'required'
      ],
      [
        'field' => 'spesialisId',
        'label' => 'spesialisId',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesLogin(){
    return [
      [
        'field' => 'nip',
        'label' => 'nip',
        'rules' => 'required'
      ],
      [
        'field' => 'password',
        'label' => 'password',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesRePassword(){
    return [
      [
        'field' => 'oldPassword',
        'label' => 'oldPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'newPassword',
        'label' => 'newPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'confirmPassword',
        'label' => 'confirmPassword',
        'rules' => 'required|matches[newPassword]'
      ]
    ];
  }

  public function showAll(){
    $spesialisId = '';
    if($_GET){
      $spesialisId = $_GET['spesialisId'];

      return $this->db->select('d.id, d.nip, d.password, d.nama, d.jk, d.alamat, d.noTelp, d.spesialisId, s.nama as namaSpesialis')
                      ->from('dokter d')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->where('d.spesialisId', $spesialisId  )
                      ->get()
                      ->result();
    }else{
      return $this->db->select('d.id, d.nip, d.password, d.nama, d.jk, d.alamat, d.noTelp, d.spesialisId, s.nama as namaSpesialis')
                      ->from('dokter d')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->get()
                      ->result();
    }
    
  }

  public function find($id){
    return $this->db->where('id', $id)->get('dokter')->row();
  }

  public function findData($id){
    return $this->db->select('d.id, d.nip, d.nama, d.jk, d.alamat, d.noTelp, d.spesialisId, s.nama as namaSpesialis')
                      ->from('dokter d')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->where('d.id', $id)
                      ->get()
                      ->row();
  }

  public function countData(){
    return $this->db->get('dokter')->num_rows();
  }

  public function store(){
    $post = $this->input->post();
    $this->nip = $post['nip'];
    $this->password = sha1('12345');
    $this->nama = $post['nama'];
    $this->jk = $post['jk'];
    $this->alamat = $post['alamat'];
    $this->noTelp = $post['noTelp'];
    $this->spesialisId = $post['spesialisId'];

    return $this->db->insert('dokter', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'nip' => $post['nip'],
      'nama' => $post['nama'],
      'jk' => $post['jk'],
      'noTelp' => $post['noTelp'],
      'alamat' => $post['alamat'],
      'spesialisId' => $post['spesialisId']
    );

    return $this->db->where('id', $post['id'])->update('dokter', $data);
  }

  public function resetPassword(){
    $post = $this->input->post();
    $data = array(
      'password' => sha1('12345')
    );

    return $this->db->where('id', $post['id'])->update('dokter', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('dokter');
  }

  // auth
  public function login(){
    $post = $this->input->post();
    $nip = $post['nip'];
    $password = sha1($post['password']);
    return $this->db->where('nip', $nip)->where('password', $password)->get('dokter')->row();
  }

  public function checkPassword(){
    $post = $this->input->post();
    $nip = $this->session->userdata('nip');
    $password = sha1($post['oldPassword']);

    $dokter = $this->db->where('nip', $nip)->where('password', $password)->get('dokter')->row();
    if(!empty($dokter)){
      return true;
    }else{
      return false;
    }
  }

  public function rePassword(){
    $post = $this->input->post();
    $array = array(
      'password' => sha1($post['newPassword'])
    );

    return $this->db->where('id', $this->session->userdata('id'))->update('dokter', $array);
  }
}
