<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spesialis_model extends CI_Model {

  public $nama;

	public function rules(){
    $id = '|is_unique[spesialis.inisial]';
    if(!empty($this->input->post('id'))){
      $id = '';
    }

    return [
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ],
      [
        'field' => 'inisial',
        'label' => 'inisial',
        'rules' => 'required'.$id
      ]
    ];
  }

  public function showAll(){
    return $this->db->get('spesialis')->result();
  }

  public function find($id){
    return $this->db->where('id', $id)->get('spesialis')->row();
  }

  public function store(){
    $post = $this->input->post();
    $this->nama = $post['nama'];
    $this->inisial = $post['inisial'];

    return $this->db->insert('spesialis', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'nama' => $post['nama'],
      'inisial' => $post['inisial']
    );

    return $this->db->where('id', $post['id'])->update('spesialis', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('spesialis');
  }
}
