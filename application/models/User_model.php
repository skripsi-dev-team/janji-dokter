<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  public $username;
  public $password;
  public $nama;

	public function rules(){
    $id = '|is_unique[admin.username]';
    if(!empty($this->input->post('id'))){
      $id = '';
    }

    return [
      [
        'field' => 'username',
        'label' => 'username',
        'rules' => 'required'.$id
      ],
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesLogin(){
    return [
      [
        'field' => 'username',
        'label' => 'username',
        'rules' => 'required'
      ],
      [
        'field' => 'password',
        'label' => 'password',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesRePassword(){
    return [
      [
        'field' => 'oldPassword',
        'label' => 'oldPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'newPassword',
        'label' => 'newPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'confirmPassword',
        'label' => 'confirmPassword',
        'rules' => 'required|matches[newPassword]'
      ]
    ];
  }

  public function showAll(){
    return $this->db->get('admin')->result();
  }

  public function find($id){
    return $this->db->where('id', $id)->get('admin')->row();
  }

  public function store(){
    $post = $this->input->post();
    $this->username = $post['username'];
    $this->password = sha1('12345');
    $this->nama = $post['nama'];

    return $this->db->insert('admin', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'username' => $post['username'],
      'nama' => $post['nama']
    );

    return $this->db->where('id', $post['id'])->update('admin', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('admin');
  }

  public function login(){
    $post = $this->input->post();
    $username = $post['username'];
    $password = sha1($post['password']);
    return $this->db->where('username', $username)->where('password', $password)->get('admin')->row();
  }

  public function checkPassword(){
    $post = $this->input->post();
    $username = $this->session->userdata('username');
    $password = sha1($post['oldPassword']);

    $user = $this->db->where('username', $username)->where('password', $password)->get('admin')->row();
    if(!empty($user)){
      return true;
    }else{
      return false;
    }
  }

  public function rePassword(){
    $post = $this->input->post();
    $array = array(
      'password' => sha1($post['newPassword'])
    );

    return $this->db->where('id', $this->session->userdata('id'))->update('admin', $array);
  }
}
