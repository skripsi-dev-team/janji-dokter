<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends CI_Model {

  public $nomer;
  public $tgl;
  public $status;
  public $dokterId;
  public $janjiId;

	public function rules(){
    return [
      [
        'field' => 'tgl',
        'label' => 'tgl',
        'rules' => 'required'
      ],
      [
        'field' => 'dokterId',
        'label' => 'dokterId',
        'rules' => 'required'
      ],
      [
        'field' => 'janjiId',
        'label' => 'janjiId',
        'rules' => 'required|numeric'
      ]
    ];
  }

  public function showAll(){
    $tgl = date('Y-m-d');
    $status = '1';
    if($_GET){
      $tgl = $_GET['tgl'];
      $status = $_GET['status'];
    }

    return $this->db->select('j.id, p.nomerRegistrasi, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->where('j.tgl >=', $tgl)
                      ->like('j.status', $status)
                      ->get()
                      ->result();
  }

  public function showAllData(){
    $tgl = date('Y-m-d');
    $status = '1';
    if($_GET){
      $tgl = $_GET['tgl'];
      $status = $_GET['status'];
    }

    if($this->session->userdata('level') == '2'){
      return $this->db->select('j.id, j.nomer, p.nomerRegistrasi, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                        ->from('jadwal j')
                        ->join('dokter d', 'd.id = j.dokterId')
                        ->join('spesialis s', 's.id = d.spesialisId')
                        ->join('janji jan', 'jan.id = j.janjiId')
                        ->join('pasien p', 'p.id = jan.pasienId')
                        ->order_by('j.tgl', 'desc')
                        ->order_by('j.nomer', 'desc')
                        ->where('j.dokterId', $this->session->userdata('id'))
                        ->where('j.tgl >=', $tgl)
                        ->like('j.status', $status)
                        ->get()
                        ->result();
    }else{
      return $this->db->select('j.id, j.nomer, j.tgl, p.nomerRegistrasi, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.id as pasienId, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->where('pasienId', $this->session->userdata('id'))
                      ->where('j.tgl >=', $tgl)
                      ->like('j.status', $status)
                      ->get()
                      ->result();
    }
  }

  public function find($id){
    return $this->db->select('j.id, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.id as pasienId, p.nama as namaPasien, s.nama as namaSpesialis, jan.tgl as tglJanji, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->where('j.id', $id)
                      ->get()
                      ->row();
  }

  public function nomerAntrian(){
    if($_GET){
      $id = $this->input->get('id');
    }else{
      $spesialis = $this->db->get('spesialis')->row();
      $id = $spesialis->id;
    }

    return $this->db->select('jadwal.id, tgl, nomer, dokter.spesialisId')
                      ->from('jadwal')
                      ->join('dokter', 'dokter.id = jadwal.dokterId')
                      ->where('dokter.spesialisId', $id)
                      ->where('tgl', date('Y-m-d'))
                      ->where('jadwal.status', '1')
                      ->order_by('jadwal.id', 'asc')
                      ->get()
                      ->row();
  }

  public function jmlPasien(){
    if($_GET){
      $id = $this->input->get('id');
    }else{
      $spesialis = $this->db->get('spesialis')->row();
      $id = $spesialis->id;
    }

    return $this->db->select('jadwal.id, tgl, nomer, dokter.spesialisId')
                      ->from('jadwal')
                      ->join('dokter', 'dokter.id = jadwal.dokterId')
                      ->where('dokter.spesialisId', $id)
                      ->where('tgl', date('Y-m-d'))
                      ->where('jadwal.status', '1')
                      ->order_by('jadwal.id', 'asc')
                      ->get()
                      ->result();
  }

  public function nomerAntrianPasien($id){
    return $this->db->select('jad.id, jad.nomer, jad.tgl, jad.status, jan.pasienId')
                      ->from('jadwal jad')
                      ->join('janji jan', 'jan.id = jad.janjiId')
                      ->where('jad.status', '1')
                      ->where('jan.pasienId', $id)
                      ->where('jan.tgl >=', date('Y-m-d'))
                      ->order_by('jad.nomer', 'asc')
                      ->get()
                      ->row();
  }

  public function store(){
    $post = $this->input->post();

    $this->nomer = $this->nomer($post['tgl'], $post['dokterId']);
    $this->tgl = $post['tgl'];
    $this->status = '1';
    $this->dokterId = $post['dokterId'];
    $this->janjiId = $post['janjiId'];

    return $this->db->insert('jadwal', $this);
  }

  public function update(){
    $post = $this->input->post();
    $nomer = $post['nomer'];
    if($post['tglOld'] != $post['tgl']){
      $nomer = $this->nomer($post['tgl'], $post['dokterId']);
    }

    $data = array(
      'nomer' => $nomer,
      'tgl' => $post['tgl'],
      'dokterId' => $post['dokterId'],
      'janjiId' => $post['janjiId']
    );

    return $this->db->where('id', $post['id'])->update('jadwal', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('jadwal');
  }

  // nomer antrean
  public function nomer($tgl, $dokterId){
    $dokter = $this->db->where('id', $dokterId)->get('dokter')->row();
    $spesialis = $this->db->where('id', $dokter->spesialisId)->get('spesialis')->row();
    
    $no = $spesialis->inisial.'01';
    // $jadwal = $this->db->where('tgl', $tgl)->order_by('id', 'desc')->get('jadwal')->row();
    $jadwal = $this->db->select('jadwal.id, tgl, nomer, dokter.spesialisId')
                        ->from('jadwal')
                        ->join('dokter', 'dokter.id = jadwal.dokterId')
                        ->where('dokter.spesialisId', $spesialis->id)
                        ->where('tgl', $tgl)
                        ->order_by('jadwal.id', 'desc')
                        ->get()
                        ->row();

    if(empty($jadwal)){
      return $no;
      // print_r($no);
      // die();
    }else{
      $array = explode($spesialis->inisial, $jadwal->nomer);
      (float)$array[1] += 1;
      if(strlen($array[1]) == '1'){
        $array[1] = '0'.$array[1];
      }

      return $spesialis->inisial.$array[1];
      // print_r($spesialis->inisial.$array[1]);
      // die();
    }
  }

  // set status
  public function setStatus(){
    $post = $this->input->post();
    $array = array(
      'status' => '0'
    );
    
    return $this->db->where('id', $post['jadwalId'])->update('jadwal', $array);
  }

  public function setStatusDelete(){
    $post = $this->input->post();
    $data = $this->db->where('id', $post['id'])->get('rekam_medis')->row();
    $array = array(
      'status' => '1'
    );
    
    return $this->db->where('id', $data->jadwalId)->update('jadwal', $array);
  }
}
