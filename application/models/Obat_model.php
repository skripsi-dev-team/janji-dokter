<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model {

  public $nama;
  public $qty;
  public $keterangan;
  public $rekamMedisId;

  public function findList($id){
    return $this->db->where('rekamMedisId', $id)->get('obat')->result();
  }

  public function store(){
    $post = $this->input->post();
    $rekamMedis = $this->db->order_by('id', 'desc')->get('rekam_medis')->row();
    if(!empty($post['obatNama'])){
      for($i = 0; $i < count($post['obatNama']); $i++){
        $this->nama = $post['obatNama'][$i];
        $this->qty = $post['obatQty'][$i];
        $this->keterangan = $post['obatKeterangan'][$i];
        $this->rekamMedisId = $rekamMedis->id;

        $this->db->insert('obat', $this);
      }
    }

    return true;
  }
}
