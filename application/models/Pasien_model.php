<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_model extends CI_Model {

  public $nomerRegistrasi;
  public $nik;
  public $password;
  public $nama;
  public $tmptLahir;
  public $tglLahir;
  public $jk;
  public $noTelp;
  public $alamat;

	public function rules(){
    $id = '|is_unique[pasien.nik]';
    if(!empty($this->input->post('id'))){
      $id = '';
    }

    return [
      
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ],
      [
        'field' => 'tmptLahir',
        'label' => 'tmptLahir',
        'rules' => 'required'
      ],
      [
        'field' => 'tglLahir',
        'label' => 'tglLahir',
        'rules' => 'required'
      ],
      [
        'field' => 'jk',
        'label' => 'jk',
        'rules' => 'required'
      ],
      [
        'field' => 'alamat',
        'label' => 'alamat',
        'rules' => 'required'
      ],
      [
        'field' => 'noTelp',
        'label' => 'noTelp',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesLogin(){
    return [
      [
        'field' => 'nomerRegistrasi',
        'label' => 'nomerRegistrasi',
        'rules' => 'required'
      ],
      [
        'field' => 'password',
        'label' => 'password',
        'rules' => 'required'
      ]
    ];
  }

  public function rulesRePassword(){
    return [
      [
        'field' => 'oldPassword',
        'label' => 'oldPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'newPassword',
        'label' => 'newPassword',
        'rules' => 'required'
      ],
      [
        'field' => 'confirmPassword',
        'label' => 'confirmPassword',
        'rules' => 'required|matches[newPassword]'
      ]
    ];
  }

  public function showAll(){
    return $this->db->get('pasien')->result();
  }

  public function find($id){
    return $this->db->where('id', $id)->get('pasien')->row();
  }

  public function findLast(){
    return $this->db->order_by('id', 'desc')->get('pasien')->row();
  }

  public function countData(){
    return $this->db->get('pasien')->num_rows();
  }

  public function store(){
    $post = $this->input->post();
    $this->nomerRegistrasi = $this->nomerRegistrasi();
    $this->nik = $post['nik'];
    $this->password = sha1('12345');
    $this->nama = $post['nama'];
    $this->tmptLahir = $post['tmptLahir'];
    $this->tglLahir = $post['tglLahir'];
    $this->jk = $post['jk'];
    $this->noTelp = $post['noTelp'];
    $this->alamat = $post['alamat'];

    return $this->db->insert('pasien', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'nik' => $post['nik'],
      'nama' => $post['nama'],
      'tmptLahir' => $post['tmptLahir'],
      'tglLahir' => $post['tglLahir'],
      'jk' => $post['jk'],
      'noTelp' => $post['noTelp'],
      'alamat' => $post['alamat']
    );

    return $this->db->where('id', $post['id'])->update('pasien', $data);
  }

  public function resetPassword(){
    $post = $this->input->post();
    $data = array(
      'password' => sha1('12345')
    );

    return $this->db->where('id', $post['id'])->update('pasien', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('pasien');
  }

  // auth
  public function login(){
    $post = $this->input->post();
    $nomerRegistrasi = $post['nomerRegistrasi'];
    $password = sha1($post['password']);
    return $this->db->where('nomerRegistrasi', $nomerRegistrasi)->where('password', $password)->get('pasien')->row();
  }

  public function checkPassword(){
    $post = $this->input->post();
    $nik = $this->session->userdata('nik');
    $password = sha1($post['oldPassword']);

    $pasien = $this->db->where('nik', $nik)->where('password', $password)->get('pasien')->row();
    if(!empty($pasien)){
      return true;
    }else{
      return false;
    }
  }

  public function rePassword(){
    $post = $this->input->post();
    $array = array(
      'password' => sha1($post['newPassword'])
    );

    return $this->db->where('id', $this->session->userdata('id'))->update('pasien', $array);
  }

  public function nomerRegistrasi(){
    $nomer = 'R-000001';
    $pasien = $this->db->order_by('id', 'desc')->get('pasien')->row();
    if(!empty($pasien)){
      $nomer = $pasien->nomerRegistrasi;
      $arrayNomer = explode('-', $nomer);
      $set = $arrayNomer[1]+1;
      
      if(strlen($set) == 1){
        $set = '00000'.$set;
      }elseif(strlen($set) == 2){
        $set = '0000'.$set;
      }elseif(strlen($set) == 3){
        $set = '000'.$set;
      }elseif(strlen($set) == 4){
        $set = '00'.$set;
      }elseif(strlen($set) == 5){
        $set = '0'.$set;
      }

      $nomer = $arrayNomer[0].'-'.$set;
    }

    return $nomer;
  }
}
