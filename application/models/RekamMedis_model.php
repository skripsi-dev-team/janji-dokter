<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekamMedis_model extends CI_Model {

  public $pasienId;
  public $jadwalId;
  public $catatan;

	public function rules(){
    return [
      [
        'field' => 'pasienId',
        'label' => 'pasienId',
        'rules' => 'required'
      ],
      [
        'field' => 'jadwalId',
        'label' => 'jadwalId',
        'rules' => 'required'
      ],
      [
        'field' => 'catatan',
        'label' => 'catatan',
        'rules' => 'required'
      ]
    ];
  }

  public function showAll(){
    return $this->db->select('rm.id, rm.pasienId, p.nama as namaPasien, p.tmptLahir, p.tglLahir, p.jk, p.alamat, p.noTelp')
                      ->from('rekam_medis rm')
                      ->join('pasien p', 'p.id = rm.pasienId')
                      ->order_by('namaPasien', 'asc')
                      ->group_by('rm.pasienId')
                      ->get()
                      ->result();
  }

  public function showAllPasien($pasienId){
    return $this->db->select('rm.id, rm.pasienId, p.nama as namaPasien, p.tmptLahir, p.tglLahir, p.jk, p.alamat, p.noTelp')
                      ->from('rekam_medis rm')
                      ->join('pasien p', 'p.id = rm.pasienId')
                      ->order_by('namaPasien', 'asc')
                      ->group_by('rm.pasienId')
                      ->where('rm.pasienId', $pasienId)
                      ->get()
                      ->result();
  }

  public function showAllDetail($pasienId){
    return $this->db->select('rm.id, rm.catatan, rm.pasienId, rm.jadwalId, jd.tgl, d.nip, d.nama as namaDokter, s.nama as namaSpesialis')
                      ->from('rekam_medis rm')
                      ->join('jadwal jd', 'jd.id = rm.jadwalId')
                      ->join('dokter d', 'd.id = jd.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->order_by('jd.tgl', 'desc')
                      ->where('rm.pasienId', $pasienId)
                      ->get()
                      ->result();
  }

  public function find($id){
    return $this->db->select('rm.id, rm.pasienId, jd.tgl, d.nip, d.nama as namaDokter, s.nama as namaSpesialis, jan.keterangan, rm.catatan')
                      ->from('rekam_medis rm')
                      ->join('jadwal jd', 'jd.id = rm.jadwalId')
                      ->join('dokter d', 'd.id = jd.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = jd.janjiId')
                      ->order_by('jd.tgl', 'desc')
                      ->where('rm.id', $id)
                      ->get()
                      ->row();
  }

  public function store(){
    $post = $this->input->post();
    $this->pasienId = $post['pasienId'];
    $this->jadwalId = $post['jadwalId'];
    $this->catatan = $post['catatan'];

    return $this->db->insert('rekam_medis', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'pasienId' => $post['pasienId'],
      'jadwalId' => $post['jadwalId'],
      'catatan' => $post['catatan']
    );

    return $this->db->where('id', $post['id'])->update('rekam_medis', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('rekam_medis');
  }
}
