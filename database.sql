create database skripsi_janjidokter
use skripsi_janjidokter

drop database skripsi_janjidokter

CREATE table pasien(
	id int primary key AUTO_INCREMENT,
	nik varchar(20),
	password varchar(50),
	nama varchar(50),
	tmptLahir varchar(50),
	tglLahir date,
	jk enum('L', 'P'),
	alamat text,
	noTelp varchar(16)
)

create table spesialis(
	id int primary key AUTO_INCREMENT,
	nama varchar(50)
)

create table dokter(
	id int primary key AUTO_INCREMENT,
	nip varchar(20),
	password varchar(50),
	nama varchar(50),
	alamat text,
	noTelp varchar(16),
	spesialisId int,
	constraint fk_spesialis_dokter foreign key(spesialisId) REFERENCES spesialis(id) ON UPDATE CASCADE
)

create table admin(
	id int primary key AUTO_INCREMENT,
	username varchar(50),
	password varchar(50)
)

create table janji(
	id int primary key AUTO_INCREMENT,
	nomer varchar(10),
	tgl date,
	keterangan text,
	pasienId int,
	spesialisId int,
	constraint fk_pasien_janji foreign key(pasienId) REFERENCES pasien(id) ON UPDATE CASCADE,
	constraint fk_spesialis_janji foreign key(spesialisId) REFERENCES spesialis(id) ON UPDATE CASCADE
)

create table jadwal(
	id int primary key AUTO_INCREMENT,
	nomer varchar(10),
	tgl date,
	status enum('1', '0'),
	dokterId int, 
	janjiId int,
	constraint fk_dokter_jadwal foreign key(dokterId) REFERENCES dokter(id) ON UPDATE CASCADE,
	constraint fk_janji_jadwal foreign key(janjiId) REFERENCES janji(id) ON UPDATE CASCADE
)

create table rekam_medis(
	id int primary key AUTO_INCREMENT,
	pasienId int,
	jadwalId int,
	catatan text,
	constraint fk_jadwal_rekam_medis foreign key(jadwalId) REFERENCES jadwal(id) ON UPDATE CASCADE,
	constraint fk_pasien_rekam_medis foreign key(pasienId) REFERENCES pasien(id) ON UPDATE CASCADE
)

create table detail_rekam_medis(
	id int primary key AUTO_INCREMENT,
	catatan text,
	jadwalId int,
	rekamMedisId int,
	constraint fk_jadwal_detail foreign key(jadwalId) REFERENCES jadwal(id) ON UPDATE CASCADE,
	constraint fk_rekam_medis_detail foreign key(rekamMedisId) REFERENCES rekam_medis(id) ON UPDATE CASCADE
)

SELECT `rm`.`id`, `rm`.`pasienId`, `jd`.`tgl d`.`nama as namaDokter s`.`nama` as `namaSpesialis` FROM `rekam_medis` `rm` JOIN `jadwal` `jd` ON `jd`.`id` = `rm`.`jadwalId` JOIN `dokter` `d` ON `d`.`id` = `jd`.`dokterId` WHERE `rm`.`pasienId` = '2' ORDER BY `jd`.`tgl` DESC